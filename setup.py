from setuptools import setup, find_packages
import os

version = open(os.path.join('mcmaster',
                            'sites',
                            'fammed',
                            'version.txt'
                            )).read().strip()

setup(name='mcmaster.sites.fammed',
      version=version,
      description='Department of Family Medicine main site policies and customizations.',
      long_description=open('README.txt').read() + '\n' +
                       open(os.path.join('docs', 'HISTORY.txt')).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
          'Framework :: Plone',
          'Programming Language :: Python',
      ],
      keywords='',
      author='Servilio Afre Puentes',
      author_email='afrepues@mcmaster.ca',
      url='http://plone.mcmaster.ca/software/',
      license='GPLv3+',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['mcmaster', 'mcmaster.sites'],
      include_package_data=True,
      zip_safe=False,
      setup_requires=[
          'setuptools-git >= 0.3',
      ],
      install_requires=[
          'setuptools',
          'Products.CMFPlone >=4.1,<5',
          'Products.GenericSetup >=1.6',
          'Products.PloneFormGen',
          'Products.PressRoom',
          'Products.FacultyStaffDirectory',
          'quintagroup.plonecaptchas',
          'quintagroup.pfg.captcha',
          'mcmaster.branding.theme',
          'collective.panels',
          'Solgema.PortletsManager',
          'redturtle.portlet.collection',
          'Products.Carousel',
          'Products.CMFBibliographyAT',
          'uwosh.pfg.d2c',
          'sc.contentrules.movebyattribute',
      ],
      entry_points={
          'z3c.autoinclude.plugin': ['target = plone'],
      },
      )
