jQuery(function($){
    // Display full bio in-page
    $('#content .facultyStaffDirectory .listing > div:first-child a').prepOverlay({
       subtype: 'ajax',
       filter: '#content',
    });
    $('#content .facultyStaffDirectory .listing .moreInfo a').prepOverlay({
       subtype: 'ajax',
       filter: '#content',
    });
});
