"""
Interfaces specific to this add-on.
"""


from plone.theme.interfaces import IDefaultPloneLayer


class ILayer(IDefaultPloneLayer):
    """A marker interface specific to this website."""
